#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>

#define PORT 9002

int main(int argc, char **argv) {
    // Define variables
    int network_socket;
    char client_message[] = "Hello World";
    int connection;
    int message_confirmation;
    struct sockaddr_in network_address;

    // Create the Socket
    network_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (network_socket == -1) {
        printf("Failed to create socket");
        return 1;
    } else {
        printf("Socket created");
    }

    // Give address to socket
    network_address.sin_port = htons(PORT);
    network_address.sin_family = AF_INET;
    network_address.sin_addr.s_addr = INADDR_ANY;

    // Connect the Socket
    connection = connect(network_socket, (struct sockaddr *) &network_address, sizeof network_address);

    if (connection == -1) {
        printf("Failed to connect to host \n");
        return 1;
    } {
        printf("Connected to host");
    }

    // Send Message
    message_confirmation = send(network_socket, client_message, sizeof client_message, 0);

    if (message_confirmation == -1) {
        printf("Failed to send message \n");
    } else {
        printf("Message sent \n");
    }

    // Close socket
    close(network_socket);

    return 0;
}
