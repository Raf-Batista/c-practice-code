#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>6
#include <unistd.h>
#include <strings.h>

#define PORT 9002

int main(int argc, char **argv) {
    // define variables
    int server_socket;
    int port_confirmation;
    int client_socket;
    char client_message[256];
    struct sockaddr_in server_address;

    // create socket
    server_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (server_socket == -1) {
        printf("Failed to create socket");
        return 1;
    } else {
        printf("Socket Created... \n");
    }

    // add address to socket

    server_address.sin_port = htons(PORT);
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;

    // bind port

    if ( (bind(server_socket, (struct sockaddr *) &server_address, sizeof server_address)) != 0) {
        printf("Failed to bind port");
        return 1;
    } else {
        printf("Port bound to %d \n", PORT);
    }

    // listen

    if ( (listen(server_socket, 5)) == -1) {
        printf("Failed to listen on port %d \n", PORT);
        return 1;
    } else {
        printf("Listening on port %d \n", PORT);
    }

    // Accept connection to our server socket
    client_socket = accept(server_socket, NULL, NULL);

    if (client_socket == -1) {
        printf("Failed to accept connection \n");
        return 1;
    } else {
        printf("Connection accepted \n");
    }

    // receive message
    recv(client_socket, &client_message, sizeof client_message, 0);

    printf("Message from client: %s \n", client_message);

    // close socket
    close(server_socket);

    return 0;
}
